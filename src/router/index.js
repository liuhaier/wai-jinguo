import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/index.vue'
const Layout = () => import("../layout/index.vue");
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: Layout,
      children: [
        {
          path: '',
          name: '/',
          component: HomeView
        },
        {
          path: '/posts',
          name: 'posts',
          // route level code-splitting
          // this generates a separate chunk (About.[hash].js) for this route
          // which is lazy-loaded when the route is visited.
          component: () => import('../views/posts.vue')
        }
      ]
    }
  ]
})

export default router
